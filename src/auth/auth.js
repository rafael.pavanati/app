import axios from "axios";
import Vue from "vue";

const api = axios.create({
    baseURL: process.env.ROOT_API ? process.env.ROOT_API : "http://localhost:3000"
});

api.interceptors.request.use(req => {
    req.headers = req.headers || {};
    req.headers["Content-Type"] = `application/json;charset=UTF-8`;
    req.headers["Access-Control-Allow-Origin"] = `*`;
    return req;
});

api.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        if (error.response && error.response.status === 500) {
            if (error.response.data && error.response.data.message) {
                Vue.notify({
                    group: "foo",
                    type: "warn",
                    title: "Dados invalidos",
                    text: error.response.data.message
                });
            }
        }
        return Promise.reject(error);
    }
);
export default api;
