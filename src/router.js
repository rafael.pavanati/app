import Vue from "vue";
import Router from "vue-router";
import Caixa from "@/components/Caixa";
import MainFooter from "@/components/layout/MainFooter";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "*",
      redirect: "/index"
    },
    {
      path: "/",
      name: "index",
      components: { default: Caixa, footer: MainFooter },
    }]}
);
